#include "communication.h"

Communication::Communication(): socket(new TSocket("localhost", 9090)),transport(new TBufferedTransport(socket)),protocol(new TBinaryProtocol(transport)),client(protocol)
{
	loggedIn = false;
	try{
		transport->open();
		client.connect();
	}
	catch (...) {
		std::cout<<"sypie sie"<<std::endl;
	}
}

Communication::~Communication()
{
	try{
		if(loggedIn)
			client.logOut();
		client.disconnect();
		transport->close();
	}
	catch (...) {}
}

void Communication::getServices(std::vector<std::string> &ref)
{
	try{
		client.listServices(ref);		
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

void Communication::getLogInForm(const std::string &service, std::string &_return)
{
	this->service=service;
	try{
		client.getLogInForm(_return,service);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

bool Communication::logIn(const std::string &params)
{
	try{
		loggedIn = true;
		return client.logIn(params,service);
	}
	catch(TApplicationException ex)
	{
		loggedIn = false;
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}

bool Communication::logOut()
{
	try{
		loggedIn = false;
		return client.logOut();
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}

void Communication::listProjects(std::vector<std::string> &ref)
{
	try{
		client.listProjects(ref);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

void Communication::getProject(long long projectID, std::string &_return)
{
	try{
		client.getProject(_return,projectID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

void Communication::listTasks(std::vector<std::string> &tasks, long long projectID)
{
	try{
		client.listTasks(tasks,projectID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

void Communication::getNewProjectForm(std::string &form)
{
	try{
		client.getNewProjectForm(form);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

bool Communication::newProject(std::string &projectData)
{
	try{
		return client.newProject(projectData);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}

void Communication::getNewTasksForm(std::string &form)
{
	try{
		client.getNewTasksForm(form);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

void Communication::getTask(long long projectID, std::string &_return)
{
	try{
		client.getTask(_return,projectID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
}

bool Communication::newTask(std::string &taskData, long long projectID)
{
	try{
		return client.newTask(taskData,projectID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}

bool Communication::editProject(std::string &projectData, long long projectID)
{
	try{
		return client.editProject(projectData,projectID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}

bool Communication::editTask(std::string &taskData, long long taskID)
{
	try{
		return client.editTask(taskData,taskID);
	}
	catch(TApplicationException ex)
	{
		std::cout<<ex.getType()<<std::endl;
	}
	return false;
}
