#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#ifndef Q_MOC_RUN
#include"json/json_spirit.h"
#include"../Client/cpp/ZPR_Projekt.h"
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <iostream>
#endif

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
//Klasa obsługujšca klienta thrift-a i komunikacje z serwerem.
class Communication{
	boost::shared_ptr<TSocket> socket;
	boost::shared_ptr<TTransport> transport;
	boost::shared_ptr<TProtocol> protocol;
	ZPR_ProjektClient client;
	//Serwis który aktualnie mamy wybrany na ekranie logowania lub do którego jestemy zalogowani.
	std::string service;
	//Oznaczenie czy jestemy aktualnie zalogowani do którego serwisu.
	bool loggedIn;
public:
	//Otwiera połšczenie z serwerem.
	Communication();
	//Zamyka połšczenie z serwerem.
	~Communication();
	//Pobiera listę dostępnych serwisów.
	void getServices(std::vector<std::string> &);
	//Pobiera formularz logowania do zadanego serwisu.
	void getLogInForm(const std::string &,std::string &);
	//Loguje do serwisu z podanymi danymi.
	bool logIn(const std::string &params);
	//Wylogowuje z aktualnego serwisu.
	bool logOut();
	//Pobiera listę projektów.
	void listProjects(std::vector<std::string> &);
	//Pobiera listę zadań dla zadanego projektu.
	void listTasks(std::vector<std::string> &,long long);
	//Pobiera formularz tworzenia nowego projektu.
	void getNewProjectForm(std::string &);
	//Tworzy nowy projekt.
	bool newProject(std::string &);
	//Pobiera formularz tworzenia nowego zadania.
	void getNewTasksForm(std::string &);
	//Pobiera dane projektu o danym ID.
	void getProject(long long , std::string &_return);
	//Pobiera dane zadania o danym ID.
	void getTask(long long,std::string &);
	//Tworzy nowe zadanie do projektu o danym ID.
	bool newTask(std::string &, long long);
	//Modyfikuje projekt o danym ID.
	bool editProject(std::string &, long long);
	//Modyfikuje zadanie o danym ID.
	bool editTask(std::string &, long long);
};

#endif // COMMUNICATION_H
