#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QPushButton>
#include <QApplication>
#include <QAction>
#include <QMenu>
#include <QFormLayout>
#include <QComboBox>
#include <QTextCodec>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QScrollArea>
#include <QDateEdit>
#include "communication.h"

using namespace json_spirit;
using namespace std;

class Choice;
//Klasa b�d�ca g��wnym okienkiem klienta, obs�uguj�ca komunikacj� pomi�dzy u�ytkownikiem a serwisami.
class MainWindow : public QMainWindow
{
    Q_OBJECT
	//Wskazanie na klas� opakowuj�ca komunikacj� z serwerem.
	Communication *communication;
	//Widget wy�wietlaj�cy formularz logowania.
	QWidget *loginForm;
	//Przycisk w menu s�u��cy do wylogowania.
	QAction *logoutAction,
	//Przycisk w menu s�u�acy do powrotu do listy projekt�w.
		*homeAction;
	//ID projektu kt�rego zadania aktualnie przegl�damy.
	long long projID;
	//JSON Object przechowuj�cy dane o aktualnie przegl�danym projekcie/zadaniu
	Object currentList;
	//Aktualnie uzupe�niany formularz.
	vector<pair<string,Choice *>> currentForm;
	//Wskazanie na instancje singletonu.
	static MainWindow* instance;

	//Tworzy ekran logowania.
	void logInScreen();
	//Tworzy formularz na podstawie otrzymanego JSON Value w zadanym QFormLayout.
	void createForm(Value &, QFormLayout *);
	//Tworzy list� na podstawie otrzymanego JSON Value w zadanym QFormLayout.
	void createList(Value &, QFormLayout *);
	//Konstruktor okna z zadana szeroko�ci� i wysoko�ci�.
	MainWindow(int width, int height);
	//Odczytuje dane z currentForm i zapisuje je w JSON-ie do otrzymanego stringa.
	bool readForm(string &);
	//Wst�pnie uzupe�nia formularz aktualnymi danymi przy edycji projektu/zadania.
	void fillForm();
	//Przygotowuje g��wne okno do wstawienia formularza (opr�cz formularza logowania).
	void prepareForm(QWidget *,const string &);
public:
	//Zwraca wskazanie na g��wne okno programu.
	static MainWindow* getInstance();
	//Wy�wietla dane projektu o danym ID.
	void getProject(long long);
	//Wy�wietla list� zada� dla projektu o danym ID.
	void listTasks(long long);
	//Wy�wietla dane zadania o danym ID.
	void getTask(long long);
	//Edytuje projekt o danym ID.
	void editProject(long long);
	//Edytuje zadanie o danym ID.
	void editTask(long long);
	//Wy�wietla formularz edycji projektu o danym ID.
	void editProjectForm(long long);
	//Wy�wietla formularz edycji zadania o danym ID.
	void editTaskForm(long long);
	//Destruktor g��wnego okna zamykaj�cy po��czenie z serwerem.
	~MainWindow();
private slots:
	//Tworzy formularz logowania dla wybranego serwisu.
	void serviceSelected(const QString &);
	//Wysy�a dane logowania do serwera.
	void logIn();
	//Wylogowuje z serwisu.
	void logOut();
	//Wy�wietla list� projekt�w.
	void listProjects();
	//Tworzy nowy projekt.
	void createProject();
	//Tworzy nowe zadanie.
	void createTask();
	//Wy�wietla formularz tworzenia nowego projektu.
	void newProjectForm();
	//Wy�wietla formularz tworzenia nowego zadania.
	void newTaskForm();
};

//Klasa ujednolicaj�ca interfejs QLineEdit, QComboBox i QDateEdit w celu u�atwienia tworzenia i odczytywania formularzy.
class Choice{
public:
	//Zwraca wpisany/wybrany tekst.
	virtual QString getText() =0;
	//Zwraca czy wyb�r rodzaju true/false - pomocnicze przy wykorzystaniu QComboBox do warto�ci bool.
	virtual bool isBoolean(){return false;}
	//Ustawia tekst / wybran� opcje na podan�.
	virtual void setText(const string &) =0;
};

//Klasa u�atwiaj�ca wykorzystanie QLineEdit w formularzach.
class LineEdit :public Choice, public QLineEdit
{
public:
	LineEdit(QWidget *parent) : QLineEdit(parent){}

	virtual QString getText()
	{
#ifdef WIN32
		if(hasAcceptableInput())
#endif
			return text();
		return "";
	}

	virtual void setText(const string &text)
	{
		QLineEdit::setText(QString::fromStdString(text));
	}
};

//Klasa u�atwiaj�ca wykorzystanie QComboBox w formularzach.
class ComboBox :public Choice, public QComboBox
{
	bool boolean;
public:
	ComboBox(QWidget *parent,bool boolean) : QComboBox(parent),boolean(boolean)
	{
		if(boolean)
		{
			addItem(QTextCodec::codecForLocale()->toUnicode("Tak"));
			addItem(QTextCodec::codecForLocale()->toUnicode("Nie"));
		}
	}

	virtual QString getText()
	{
		return currentText();
	}

	virtual bool isBoolean()
	{
		return boolean;
	}

	virtual void setText(const string &text)
	{
		setCurrentText(QString::fromStdString(text));
	}
};

//Klasa u�atwiaj�ca wykorzystanie QDateEdit w formularzach.
class DateEdit :public Choice, public QDateEdit
{
public:
	DateEdit(QWidget *parent) : QDateEdit(parent)	
	{
		setDisplayFormat("yyyy-MM-dd");
	}

	virtual QString getText()
	{
		return date().toString("yyyy-MM-dd");
	}

	virtual void setText(const string &text)
	{
		setDate(QDate::fromString(QString::fromStdString(text),"yyyy-MM-dd"));
	}
};

//Klasa umo�liaj�ca tworzenie QPushButton pami�taj�cego do projektu/zadania o jakim ID si� odnosi.
class PushButton : public QPushButton
{
	Q_OBJECT
	//ID projektu/zadania do kt�rego wykorzystany zosta� przycisk.
	long long ID;
public:
	//Konstruktor przycisku z danym tekstem, rodzicem oraz ID projektu/zadania.
	PushButton(const QString &text,long long ID, QWidget *parent = 0) : QPushButton(text,parent)
	{
		this->ID = ID;
	}
private slots:
	//Wy�wietla informacje o projekcie.
	void moreInfoProject()
	{
		MainWindow::getInstance()->getProject(ID);
	}

	//Wy�wietla list� zada� dla projektu.
	void tasks()
	{
		MainWindow::getInstance()->listTasks(ID);
	}

	//Wy�wietla informacje o zadaniu.
	void moreInfoTask()
	{
		MainWindow::getInstance()->getTask(ID);
	}

	//Edytuje projektu.
	void editProject()
	{
		MainWindow::getInstance()->editProject(ID);
	}

	//Edytuje zadanie.
	void editTask()
	{
		MainWindow::getInstance()->editTask(ID);
	}

	//Wy�wietla formularz edycji projektu.
	void editProjectForm()
	{
		MainWindow::getInstance()->editProjectForm(ID);
	}

	//Wy�wietla formularz edycji zadania.
	void editTaskForm()
	{
		MainWindow::getInstance()->editTaskForm(ID);
	}
};
#endif // MAINWINDOW_H
