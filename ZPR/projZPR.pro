#-------------------------------------------------
#
# Project created by QtCreator 2013-05-15T10:32:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = projZPR
TEMPLATE = app
unix:QMAKE_CXXFLAGS += -DHAVE_NETINET_IN_H\
        -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp\
        communication.cpp\
        ..\Client\cpp\ZPR_projekt.cpp\
        ..\Client\cpp\ZPR_Projekt_constants.cpp\
        ..\Client\cpp\ZPR_Projekt_types.cpp\
        json\json_spirit_reader.cpp\
        json\json_spirit_writer.cpp\

win32:SOURCES += ..\Client\thrift\windows\GetTimeOfDay.cpp\
        ..\Client\thrift\windows\TWinsockSingleton.cpp\
        ..\Client\thrift\windows\WinFcntl.cpp\
        ..\Client\thrift\TApplicationException.cpp\
        ..\Client\thrift\Thrift.cpp\
        ..\Client\thrift\transport\TBufferTransports.cpp\
        ..\Client\thrift\transport\TSocket.cpp

        
win32:INCLUDEPATH += C:\boost_1_53_0\boost_1_53_0\libs\
        C:\boost_1_53_0\boost_1_53_0\
        ..\Client\        

unix:INCLUDEPATH += /usr/include/boost\
        ../Client


win32:LIBS += C:\boost_1_53_0\boost_1_53_0\stage\lib\x86\libboost_thread-vc110-mt-1_53.lib\
        C:\boost_1_53_0\boost_1_53_0\stage\lib\x86\libboost_system-vc110-mt-1_53.lib

unix:LIBS += -lboost_thread\
        -lboost_system\
        -lthrift

HEADERS  += mainwindow.h\
        communication.h
