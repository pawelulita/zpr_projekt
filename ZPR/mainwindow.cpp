#include "mainwindow.h"

MainWindow* MainWindow::instance = nullptr;
MainWindow* MainWindow::getInstance()
{
	if(!instance)
		instance = new MainWindow(500,300);
	return instance;
}

MainWindow::MainWindow(int width, int height)
{
	loginForm=nullptr;
	communication = new Communication();
	resize(width,height);
	show();
    setMenuBar(new QMenuBar(this));
	QMenu *fileMenu = menuBar()->addMenu(QTextCodec::codecForLocale()->toUnicode("Plik"));
	QAction *exitAction = new QAction(QTextCodec::codecForLocale()->toUnicode("Zakończ"),fileMenu);
	logoutAction = new QAction(QTextCodec::codecForLocale()->toUnicode("Wyloguj"),fileMenu);
	logoutAction->setEnabled(false);
	homeAction = menuBar()->addAction(QTextCodec::codecForLocale()->toUnicode("Lista projektow"));
	homeAction->setEnabled(false);
	fileMenu->addAction(logoutAction);
	fileMenu->addAction(exitAction);
	connect(exitAction, SIGNAL(triggered()), QApplication::instance(), SLOT(quit()));
	connect(logoutAction,SIGNAL(triggered()), this, SLOT(logOut()));
	connect(homeAction,SIGNAL(triggered()), this, SLOT(listProjects()));
	setWindowTitle(QTextCodec::codecForLocale()->toUnicode("Aplikacja do zarzadzania projektami"));
	logInScreen();
}

void MainWindow::logInScreen()
{
	vector<string> services;
	setCentralWidget(new QWidget(this));
	new QVBoxLayout(centralWidget());    
	QPushButton *logIn = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Zaloguj"),centralWidget());
	QComboBox *chooseService = new QComboBox(centralWidget());
	QWidget *serviceSelection = new QWidget(centralWidget());
	new QHBoxLayout(serviceSelection);
	serviceSelection->layout()->addWidget(chooseService);
	serviceSelection->layout()->addWidget(logIn);
	centralWidget()->layout()->addWidget(serviceSelection);	
	communication->getServices(services);
	for(auto& i : services)
		chooseService->addItem(QString::fromStdString(i));
	connect(chooseService, SIGNAL(currentIndexChanged(const QString &)),this,SLOT(serviceSelected(const QString &)));
	connect(logIn,SIGNAL(pressed()),this,SLOT(logIn()));
	chooseService->currentIndexChanged(QString::fromStdString(services[0]));
}

MainWindow::~MainWindow()
{
	delete communication;
}

void MainWindow::serviceSelected(const QString &text)
{
	currentForm.clear();
	centralWidget()->layout()->removeWidget(loginForm);
	delete loginForm;
	loginForm = new QWidget(centralWidget());
	new QFormLayout(loginForm);
	centralWidget()->layout()->addWidget(loginForm);
	string form;
	communication->getLogInForm(text.toStdString(),form);
	Value value;
	read( form, value );
	createForm(value,dynamic_cast<QFormLayout*>(loginForm->layout()));
}

bool MainWindow::readForm(string &out)
{
	Object addr_obj;
	for(int i =0;i<currentForm.size();++i)
	{cout<<currentForm[i].second->getText().toStdString()<<endl;
		if(currentForm[i].second->getText().isEmpty())
		{
			QMessageBox msgBox;
			msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Uzupełnij poprawnie wszystkie pola."));
			msgBox.exec();
			return false;
		}
		if(currentForm[i].second->isBoolean())
			addr_obj.push_back(Pair(currentForm[i].first,currentForm[i].second->getText()=="Tak" ? true : false));
		else
			addr_obj.push_back(Pair(currentForm[i].first,currentForm[i].second->getText().toStdString()));
	}
	out = write( addr_obj, pretty_print );
	return true;
}

void MainWindow::logIn()
{
	string data;
	readForm(data);
	if(communication->logIn(data))
	{
		logoutAction->setEnabled(true);
		homeAction->setEnabled(true);
		loginForm = nullptr;
		listProjects();
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Nieudane logowanie."));
		msgBox.exec();
	}
}

void MainWindow::createForm(Value &value, QFormLayout *layout)
{
	const Object &a = value.get_obj();
	const Object &fields = find_value(a,"fields").get_obj();
	const Array &order= find_value(a,"order").get_array();
	QWidget *widget;
	for(auto& i : order)
	{
		Value field = find_value(fields,i.get_str());
		if(field.type()==2)
		{
			if(field.get_str() == "bool")
			{
				widget = new ComboBox(layout->parentWidget(),true);
			}
			else if(field.get_str() == "date")
			{
				widget = new DateEdit(layout->parentWidget());
			}
			else
			{
				widget = new LineEdit(layout->parentWidget());
				if(field.get_str() == "pass")
					dynamic_cast<QLineEdit *>(widget)->setEchoMode(QLineEdit::Password);
				else if(field.get_str() == "int")
				{
					dynamic_cast<QLineEdit *>(widget)->setValidator(new QIntValidator(layout->parentWidget()));
				}
			}
		}
		else if(field.type() == 1)
		{
			widget = new ComboBox(layout->parentWidget(),false);
			Array arr = field.get_array();
			for(auto& k : arr)
				dynamic_cast<QComboBox *>(widget)->addItem(QString::fromStdString(k.get_str()));
		}
		layout->addRow(new QLabel(QString::fromStdString(i.get_str()+ " : "),layout->parentWidget()),widget);
		currentForm.push_back(make_pair(i.get_str(),dynamic_cast<Choice *>(widget)));
	}
}

void MainWindow::createList(Value &value, QFormLayout *layout)
{
	const Object &a = value.get_obj();
	currentList = find_value(a,"fields").get_obj();
	const Array &order= find_value(a,"order").get_array();
	QWidget *widget;
	for(auto& i : order)
	{
		Value field = find_value(currentList,i.get_str());
		if(field.is_null())
			widget = new QLabel(QString::fromStdString(""),layout->parentWidget());
		else if(field.type() == 2)
			widget = new QLabel(QString::fromStdString(field.get_str()),layout->parentWidget());
		else if(field.type() == 4)
		{
			widget = new QLabel(QString::fromStdString(to_string(field.get_int64())),layout->parentWidget());
		}else
		{
			widget = new QLabel(QString::fromStdString(field.get_bool()?"Tak":"Nie"),layout->parentWidget());
		}
		layout->addRow(new QLabel(QString::fromStdString(i.get_str() + " : "),layout->parentWidget()),widget);
	}
}

void MainWindow::logOut()
{
	communication->logOut();
	logoutAction->setEnabled(false);
	homeAction->setEnabled(false);
	logInScreen();
}

void MainWindow::listProjects()
{
	setCentralWidget(new QScrollArea(this));
	QWidget *projectList = new QWidget(centralWidget());
	new QVBoxLayout(projectList);
	dynamic_cast<QScrollArea *>(centralWidget())->setAlignment(Qt::AlignHCenter);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidgetResizable(true);
	vector<string> projects;
	communication->listProjects(projects);
	for(auto& i : projects)
	{
		QFrame  *projectInfo = new QFrame(centralWidget());
		projectInfo->setFrameStyle(QFrame::Panel);
		new QHBoxLayout(projectInfo);
		QWidget *details = new QWidget(projectInfo),*buttons = new QWidget(projectInfo);
		dynamic_cast<QBoxLayout*>(projectInfo->layout())->addWidget(details);
		dynamic_cast<QBoxLayout*>(projectInfo->layout())->addWidget(buttons);
		new QFormLayout(details);
		new QVBoxLayout(buttons);
		Value value;
		read( i, value );
		Object &a = value.get_obj();
		dynamic_cast<QFormLayout *>(details->layout())->addRow(new QLabel(QString::fromStdString(a[0].name_+ " : "),details),new QLabel(QString::fromStdString(to_string(a[0].value_.get_int64())),details));
		dynamic_cast<QFormLayout *>(details->layout())->addRow(new QLabel(QString::fromStdString(a[1].name_+ " : "),details),new QLabel(QString::fromStdString(a[1].value_.get_str()),details));
		PushButton *moreInfo = new PushButton(QTextCodec::codecForLocale()->toUnicode("Szczegóły"),a[0].value_.get_int64(),buttons);
		connect(moreInfo,SIGNAL(pressed()),moreInfo,SLOT(moreInfoProject()));
		PushButton *tasks = new PushButton(QTextCodec::codecForLocale()->toUnicode("Zadania"),a[0].value_.get_int64(),buttons);
		connect(tasks,SIGNAL(pressed()),tasks,SLOT(tasks()));
		dynamic_cast<QBoxLayout *>(buttons->layout())->addWidget(moreInfo);
		dynamic_cast<QBoxLayout *>(buttons->layout())->addWidget(tasks);		
		projectList->layout()->addWidget(projectInfo);
	}
	QPushButton *newProject = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Nowy projekt"),projectList);
	projectList->layout()->addWidget(newProject);
	connect(newProject,SIGNAL(pressed()),this,SLOT(newProjectForm()));
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(projectList);
}

void MainWindow::getProject(long long projectId)
{
	setCentralWidget(new QScrollArea(this));
	QWidget *projectInfo = new QWidget(centralWidget());
	new QFormLayout(projectInfo);
	string project;
	communication->getProject(projectId,project);
	Value value;
	read(project,value);
	createList(value,dynamic_cast<QFormLayout *>(projectInfo->layout()));
	PushButton *edit = new PushButton(QTextCodec::codecForLocale()->toUnicode("Edycja"),projectId,projectInfo);
	QPushButton *_return = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projectInfo);
	dynamic_cast<QFormLayout *>(projectInfo->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(projectInfo);
	connect(edit,SIGNAL(pressed()),edit,SLOT(editProjectForm()));
	connect(_return,SIGNAL(pressed()),this,SLOT(listProjects()));
}

void MainWindow::getTask(long long taskID)
{
	setCentralWidget(new QScrollArea(this));
	QWidget *taskInfo = new QWidget(centralWidget());
	new QFormLayout(taskInfo);
	string task;
	communication->getTask(taskID,task);
	Value value;
	read(task,value);
	createList(value,dynamic_cast<QFormLayout *>(taskInfo->layout()));
	PushButton *edit = new PushButton(QTextCodec::codecForLocale()->toUnicode("Edycja"),taskID,taskInfo);
	PushButton *_return = new PushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projID,taskInfo);
	dynamic_cast<QFormLayout *>(taskInfo->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(taskInfo);
	connect(edit,SIGNAL(pressed()),edit,SLOT(editTaskForm()));
	connect(_return,SIGNAL(pressed()),_return,SLOT(tasks()));
}

void MainWindow::listTasks(long long projectId)
{
	projID = projectId;
	setCentralWidget(new QScrollArea(this));
	QWidget *tasksList = new QWidget(centralWidget());
	new QVBoxLayout(tasksList);
	dynamic_cast<QScrollArea *>(centralWidget())->setAlignment(Qt::AlignHCenter);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidgetResizable(true);
	vector<string> tasks;
	communication->listTasks(tasks,projectId);
	for(auto& i : tasks)
	{
		QFrame  *taskInfo = new QFrame(centralWidget());
		taskInfo->setFrameStyle(QFrame::Panel);
		new QHBoxLayout(taskInfo);
		QWidget *details = new QWidget(taskInfo),*buttons = new QWidget(taskInfo);
		dynamic_cast<QBoxLayout*>(taskInfo->layout())->addWidget(details);
		dynamic_cast<QBoxLayout*>(taskInfo->layout())->addWidget(buttons);
		new QFormLayout(details);
		new QVBoxLayout(buttons);
		Value value;
		read( i, value );
		Object &a = value.get_obj();
		const Object &fields = find_value(a,"fields").get_obj();
		const Array &order= find_value(a,"order").get_array();
		PushButton *moreInfo;
		for(auto& j : order)
		{
			Value field = find_value(fields,j.get_str());
			if(field.type()==2)
			{				
				dynamic_cast<QFormLayout *>(details->layout())->addRow(new QLabel(QString::fromStdString(j.get_str() + " : "),details),new QLabel(QString::fromStdString(field.get_str()),details));
			}
			else 
			{
				if(j.get_str() == "ID")
					 moreInfo = new PushButton(QTextCodec::codecForLocale()->toUnicode("Szczegóły"),field.get_int64(),buttons);
				dynamic_cast<QFormLayout *>(details->layout())->addRow(new QLabel(QString::fromStdString(j.get_str()+ " : "),details),new QLabel(QString::fromStdString(to_string(field.get_int64())),details));
			}
		}
		connect(moreInfo,SIGNAL(pressed()),moreInfo,SLOT(moreInfoTask()));
		dynamic_cast<QBoxLayout *>(buttons->layout())->addWidget(moreInfo);
		tasksList->layout()->addWidget(taskInfo);
	}
	QPushButton *newTask = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Nowe zadanie"),tasksList);
	connect(newTask,SIGNAL(pressed()),this,SLOT(newTaskForm()));
	tasksList->layout()->addWidget(newTask);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(tasksList);
}

void MainWindow::editProject(long long projectID)
{
	string data;
	if(!readForm(data))
		return;
	QMessageBox msgBox;
	if(communication->editProject(data,projectID))
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Zmodyfikowano projekt."));
	else
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Nieudało się zmodyfikować projektu."));
	msgBox.exec();
	getProject(projectID);
}

void MainWindow::editTask(long long taskID)
{
	string data;
	if(!readForm(data))
		return;
	QMessageBox msgBox;
	if(communication->editTask(data,taskID))
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Zmodyfikowano zadanie."));
	else
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Nieudało się zmodyfikować zadania."));
	msgBox.exec();
	listTasks(projID);
}

void MainWindow::createProject()
{
	string data;
	if(!readForm(data))
		return;
	QMessageBox msgBox;
	if(communication->newProject(data))
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Utworzono nowy projekt."));
	else
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Utworzenie nowego projektu nie powiodło się."));
	msgBox.exec();
	listProjects();
}

void MainWindow::createTask()
{
	string data;
	if(!readForm(data))
		return;
	QMessageBox msgBox;
	if(communication->newTask(data,projID))
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Utworzono nowe zadanie."));
	else
		msgBox.setText(QTextCodec::codecForLocale()->toUnicode("Utworzenie nowego zadania nie powiodło się."));
	msgBox.exec();
	listTasks(projID);
}

void MainWindow::newProjectForm()
{
	QWidget *projectForm = new QWidget(centralWidget());
	string form;
	communication->getNewProjectForm(form);
	this->prepareForm(projectForm,form);
	QPushButton *edit = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Utwórz"),projectForm);
	QPushButton *_return = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projectForm);
	dynamic_cast<QFormLayout *>(projectForm->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(projectForm);
	connect(edit,SIGNAL(pressed()),this,SLOT(createProject()));
	connect(_return,SIGNAL(pressed()),this,SLOT(listProjects()));
}

void MainWindow::newTaskForm()
{
	QWidget *taskForm = new QWidget(centralWidget());
	string form;
	communication->getNewTasksForm(form);
	this->prepareForm(taskForm,form);
	QPushButton *edit = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Utwórz"),taskForm);
	PushButton *_return = new PushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projID,taskForm);
	dynamic_cast<QFormLayout *>(taskForm->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(taskForm);
	connect(edit,SIGNAL(pressed()),this,SLOT(createTask()));
	connect(_return,SIGNAL(pressed()),_return,SLOT(tasks()));
}

void MainWindow::prepareForm(QWidget *form,const string &data)
{
	setCentralWidget(new QScrollArea(this));
	new QFormLayout(form);
	dynamic_cast<QScrollArea *>(centralWidget())->setAlignment(Qt::AlignHCenter);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidgetResizable(true);
	currentForm.clear();
	Value value;
	read( data, value );
	createForm(value,dynamic_cast<QFormLayout*>(form->layout()));
}

void MainWindow::editProjectForm(long long projectID)
{
	QWidget *projectForm = new QWidget(centralWidget());
	string form;
	communication->getNewProjectForm(form);
	this->prepareForm(projectForm,form);
	fillForm();
	PushButton *edit = new PushButton(QTextCodec::codecForLocale()->toUnicode("Edytuj"),projectID,projectForm);
	QPushButton *_return = new QPushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projectForm);
	dynamic_cast<QFormLayout *>(projectForm->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(projectForm);
	connect(edit,SIGNAL(pressed()),edit,SLOT(editProject()));
	connect(_return,SIGNAL(pressed()),this,SLOT(listProjects()));
}

void MainWindow::editTaskForm(long long taskID)
{	
	QWidget *taskForm = new QWidget(centralWidget());
	string form;
	communication->getNewTasksForm(form);
	this->prepareForm(taskForm,form);
	fillForm();
	PushButton *edit = new PushButton(QTextCodec::codecForLocale()->toUnicode("Edytuj"),taskID,taskForm);
	PushButton *_return = new PushButton(QTextCodec::codecForLocale()->toUnicode("Powrót"),projID,taskForm);
	dynamic_cast<QFormLayout *>(taskForm->layout())->addRow(edit,_return);
	dynamic_cast<QScrollArea *>(centralWidget())->setWidget(taskForm);
	connect(edit,SIGNAL(pressed()),edit,SLOT(editTask()));
	connect(_return,SIGNAL(pressed()),_return,SLOT(tasks()));
}

void MainWindow::fillForm()
{
	int j;
	for(auto& i : currentForm)
	{
		for(j=0;i.first != currentList[j].name_;++j);
		if(currentList[j].value_.is_null())
			continue;
		if(currentList[j].value_.type() == 2)
			i.second->setText(currentList[j].value_.get_str());
		else if(currentList[j].value_.type() == 4)
		{
			i.second->setText(to_string(currentList[j].value_.get_int64()));
		}else
		{
			i.second->setText(currentList[j].value_.get_bool()?"Tak":"Nie");
		}
	}
}
