#include "../cpp/ZPR_projekt.h"  // As an example

#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <iostream>
#include <vector>

using namespace std;

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;


int main(int argc, char **argv) {
  string asana_key = "1sEelTTm.mgd7MBShOvbdKWGpJjkn89r";
  boost::shared_ptr<TSocket> socket(new TSocket("localhost", 9090));
  boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
  boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

  ZPR_projektClient client(protocol);
  transport->open();

  client.logIn("{\"apikey\" : \"" + asana_key + "\"}", "asana");
  transport->close();

  return 0;
}
