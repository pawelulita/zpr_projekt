#include "../../ZPR/communication.h"

#include "cdk_window.h"
#include "cdk_wrapper.h"

CDKWindow::CDKWindow() : comm(new Communication())
{
	cursesWin = initscr();
	screen = initCDKScreen(cursesWin);
	initCDKColor();

	logInScreen();
}

CDKWindow::~CDKWindow()
{
	destroyCDKScreen(screen);
	endCDK();
}

CDKWindow& CDKWindow::getInstance()
{
	static CDKWindow instance;
	return instance;
}

void CDKWindow::logInScreen()
{
	std::vector<std::string> services;
	comm->getServices(services);
	CDKRadio serviceSelection(screen, CENTER, CENTER, 30, getmaxy(cursesWin)/2, "<C>Select service.");
	serviceSelection.setItems(services);

	do {
		serviceSelection.activate();
		if (serviceSelection.getExitType() == vNORMAL)
			serviceSelected(services[serviceSelection.getSelected()]);
	} while (serviceSelection.getExitType() != vESCAPE_HIT);
}

void CDKWindow::serviceSelected(const std::string& service)
{
	std::string form, data;

	comm->getLogInForm(service, form);
	CDKForm logInForm(cursesWin, "Log in to " + service, form);

	eraseCDKScreen(screen);

	while (logInForm.getData(data))
	{
		if (comm->logIn(data))
		{
			listProjects();
			comm->logOut();
			return;
		}
		else
		{
			eraseCDKScreen(screen);
			CDKLabel::popup(screen, CDKStringArray("<C> Login failed. \n<C> Please try again. "));
		}
	}
}

void CDKWindow::updateProjectList(std::vector<long long>& ids, std::vector<std::string>& names)
{
	std::vector<std::string> projects;
	comm->listProjects(projects);

	ids.clear();
	names.clear();
	for (auto p : projects)
	{
		json_spirit::Value val;
		json_spirit::read(p, val);
		ids.push_back(val.get_obj()[0].value_.get_int64());
		names.push_back(val.get_obj()[1].value_.get_str());
	}
	ids.push_back(0);
	names.push_back("</B>Create new<!B>");
}

void CDKWindow::listProjects()
{
	std::vector<long long> projectIDs;
	std::vector<std::string> projectNames;

	updateProjectList(projectIDs, projectNames);

	CDKRadio projectSelection(screen, CENTER, CENTER, getmaxx(cursesWin)/2, getmaxy(cursesWin)/2,
		"<C>Select project.\n"
		"<C></B>ENTER<!B>: tasks  </B>I<!B>: summary  </B>E<!B>: edit"
	);
	projectSelection.setItems(projectNames);

	projectSelection.addCallback([this, &projectIDs, &projectNames, &projectSelection](void* obj, chtype c) {
        if (projectSelection.getSelected() < projectIDs.size() - 1)
		{
			if (c == 'i' || c == 'I')
				getProject(projectIDs[projectSelection.getSelected()]);
			else if (c == 'e' || c == 'E')
			{
				editProject(projectIDs[projectSelection.getSelected()]);
				updateProjectList(projectIDs, projectNames);
				projectSelection.setItems(projectNames);
				refreshCDKScreen(screen);
			}
		}
		return 0;
	});

	do {
		projectSelection.activate();
		int selected = projectSelection.getSelected();

		if (projectSelection.getExitType() == vNORMAL)
		{
			if (selected == projectIDs.size() - 1)
			{
				createProject();
				updateProjectList(projectIDs, projectNames);
				projectSelection.setItems(projectNames);
				refreshCDKScreen(screen);
			}
			else
				listTasks(projectIDs[selected]);
		}
	} while (projectSelection.getExitType() != vESCAPE_HIT);
}

void CDKWindow::createProject()
{
	std::string form, data;

	comm->getNewProjectForm(form);
	CDKForm projectForm(cursesWin, "Create new project", form);

	eraseCDKScreen(screen);

	while (projectForm.getData(data))
	{
		if (comm->newProject(data))
		{
			refreshCDKScreen(screen);
			break;
		}
		else
		{
			eraseCDKScreen(screen);
			CDKLabel::popup(screen, CDKStringArray("<C> Project creation failed. \n<C> Please try again. "));
		}
	}
	refreshCDKScreen(screen);
}

void CDKWindow::editProject(long long id)
{
	std::string form, data;

	comm->getNewProjectForm(form);
	comm->getProject(id, data);
	CDKForm projectForm(cursesWin, "Edit project", form);
	projectForm.setData(data);

	eraseCDKScreen(screen);

	while (projectForm.getData(data))
	{
		if (comm->editProject(data, id))
			break;
		else
		{
			eraseCDKScreen(screen);
			CDKLabel::popup(screen, CDKStringArray("<C> Project edition failed. \n<C> Please try again. "));
		}
	}
	refreshCDKScreen(screen);
}

void CDKWindow::getProject(long long id)
{
	std::string info;
	comm->getProject(id, info);
	CDKLabel::popup(screen, info);
}

void CDKWindow::updateTaskList(long long id, std::vector<long long>& ids, std::vector<std::string>& names)
{
	std::vector<std::string> tasks;
	comm->listTasks(tasks, id);

	ids.clear();
	names.clear();
	for (auto t : tasks)
	{
		json_spirit::Value val;
		json_spirit::read(t, val);
		json_spirit::Object fields = find_value(val.get_obj(), "fields").get_obj();
		ids.push_back(json_spirit::find_value(fields, "ID").get_int64());
		names.push_back(json_spirit::find_value(fields, "Nazwa").get_str());
	}
	ids.push_back(0);
	names.push_back("</B>Create new<!B>");
}

void CDKWindow::listTasks(long long id)
{
	std::vector<long long> taskIDs;
	std::vector<std::string> taskNames;

	updateTaskList(id, taskIDs, taskNames);

	CDKRadio taskSelection(screen, CENTER, CENTER, getmaxx(cursesWin)/2, getmaxy(cursesWin)/2,
		"<C>Select task.\n"
		"<C></B>ENTER<!B>: summary  </B>E<!B>: edit"
	);
	taskSelection.setItems(taskNames);

	taskSelection.addCallback([this, &id, &taskIDs, &taskNames, &taskSelection](void* obj, chtype c) {
		if (taskSelection.getSelected() < taskIDs.size() - 1)
		{
			if (c == 'e' || c == 'E')
			{
				editTask(taskIDs[taskSelection.getSelected()]);
				updateTaskList(id, taskIDs, taskNames);
				taskSelection.setItems(taskNames);
				refreshCDKScreen(screen);
			}
		}
		return 0;
	});

	do {
		taskSelection.activate();
		int selected = taskSelection.getSelected();

		if (taskSelection.getExitType() == vNORMAL)
		{
			if (selected == taskIDs.size() - 1)
			{
				createTask(id);
				updateTaskList(id, taskIDs, taskNames);
				taskSelection.setItems(taskNames);
				refreshCDKScreen(screen);
			}
			else
				getTask(taskIDs[selected]);
		}
	} while (taskSelection.getExitType() != vESCAPE_HIT);
}

void CDKWindow::createTask(long long id)
{
	std::string form, data;

	comm->getNewTasksForm(form);
	CDKForm taskForm(cursesWin, "Create new task", form);

	eraseCDKScreen(screen);

	while (taskForm.getData(data))
	{
		if (comm->newTask(data, id))
		{
			refreshCDKScreen(screen);
			break;
		}
		else
		{
			eraseCDKScreen(screen);
			CDKLabel::popup(screen, CDKStringArray("<C> Task creation failed. \n<C> Please try again. "));
		}
	}
	refreshCDKScreen(screen);
}

void CDKWindow::editTask(long long id)
{
	std::string form, data;

	comm->getNewTasksForm(form);
	comm->getTask(id, data);
	CDKForm taskForm(cursesWin, "Edit task", form);
	taskForm.setData(data);

	eraseCDKScreen(screen);

	while (taskForm.getData(data))
	{
		if (comm->editTask(data, id))
			break;
		else
		{
			eraseCDKScreen(screen);
			CDKLabel::popup(screen, CDKStringArray("<C> Task edition failed. \n<C> Please try again. "));
		}
	}
	refreshCDKScreen(screen);
}

void CDKWindow::getTask(long long id)
{
	std::string info;
	comm->getTask(id, info);
	CDKLabel::popup(screen, info);
}
