#ifndef CDK_WINDOW_H
#define CDK_WINDOW_H

#include <string>
#include <boost/shared_ptr.hpp>

extern "C"
{
#include <cdk.h>
}

class Communication;

/**
 * Główna klasa programu, singleton.
 */
class CDKWindow
{
	public:
		/**
		 * Tworzy i zwraca bieżącą instancję aplikacji.
		 */
		static CDKWindow& getInstance();

	private:
		CDKWindow();
		~CDKWindow();

		/**
		 * Tworzy, wyświetla i obsługuje ekran wyboru serwisu.
		 */
		void logInScreen();

		/**
		 * Tworzy, wyświetla i obsługuje ekran logowania do wybranego serwisu.
		 */
		void serviceSelected(const std::string& service);

		/**
		 * Aktualizuje listę projektów założonych w wybranym serwisie.
		 *
		 * @param ids Wektor do zapisania ID istniejących projektów
		 * @param names Wektor do zapisania nazw istniejących projektów
		 */
		void updateProjectList(std::vector<long long>& ids, std::vector<std::string>& names);

		/**
		 * Tworzy, wyświetla i obsługuje ekran wyboru projektu.
		 */
		void listProjects();

		/**
		 * Tworzy, wyświetla i obsługuje formularz tworzenia nowego projektu.
		 */
		void createProject();

		/**
		 * Tworzy, wyświetla i obsługuje formularz edycji wybranego projektu.
		 *
		 * @param id ID wybranego projektu
		 */
		void editProject(long long id);

		/**
		 * Wyświetla informacje o wybranym projekcie.
		 *
		 * @param id ID wybranego projektu
		 */
		void getProject(long long id);

		/**
		 * Aktualizuje listę zadań stworzonych w wybranym projekcie.
		 * @param id ID wybranego projektu
		 * @param ids Wektor do zapisania ID istniejących zadań
		 * @param names Wektor do zapisania nazw istniejących zadań
		 */
		void updateTaskList(long long id, std::vector<long long>& ids, std::vector<std::string>& names);

		/**
		 * Tworzy, wyświetla i obsługuje ekran wyboru zadania.
		 *
		 * @param id ID wybranego projektu
		 */
		void listTasks(long long id);

		/**
		 * Tworzy, wyświetla i obsługuje formularz tworzenia nowego zadania.
		 *
		 * @param id ID wybranego projektu
		 */
		void createTask(long long id);

		/**
		 * Tworzy, wyświetla i obsługuje formularz edycji wybranego zadania.
		 *
		 * @param id ID wybranego zadania
		 */
		void editTask(long long id);

		/**
		 * Wyświetla informacje o wybranym zadaniu.
		 *
		 * @param id ID wybranego zadania
		 */
		void getTask(long long id);

		boost::shared_ptr<Communication> comm;

		WINDOW* cursesWin;
		CDKSCREEN* screen;
};

#endif
