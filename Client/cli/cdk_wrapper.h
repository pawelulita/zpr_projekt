#ifndef CDK_WRAPPER_H
#define CDK_WRAPPER_H

#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>

#include "../../ZPR/json/json_spirit.h"

#include <cdk.h>

/**
 * Klasa pomocnicza, konwertująca wielolinijkowe napisy na tablice c-stringów.
 */
struct CDKStringArray
{
	/**
	 * Konstruktor, tworzy tablicę z obiektu string.
	 *
	 * @param s Napis z 0 lub więcej wystąpień znaku nowej linii
	 */
	CDKStringArray(const std::string& s);
	/**
	 * Konstruktor, tworzy tablicę z wektora stringów.
	 *
	 * @param svec Wektor napisów tworzących pojedyncze linie
	 */
	CDKStringArray(const std::vector<std::string>& svec);

	~CDKStringArray();

	unsigned int count;
	char** data;
};

/**
 * Abstrakcyjna klasa bazowa widgetu CDK, zapewnia jednolity interfejs obiektowi formularza.
 */
class CDKWidget
{
	public:
		/**
		 * Konstruktor, tworzy widget i nadaje mu nazwę.
		 *
		 * @param name Nazwa będąca identyfikatorem widgetu
		 */
		CDKWidget(const std::string& n) : name(n) {}

		virtual ~CDKWidget() {}

		/**
		 * Przekazuje kontrolce focus i czeka na wejście od użytkownika.
		 */
		virtual void activate() = 0;

		/**
		 * Ustawia bieżącą zawartość kontrolki.
		 *
		 * @param val Obiekt wartości JSON
		 */
		virtual void setValue(const json_spirit::Value& val) = 0;

		/**
		 * Zwraca bieżącą zawartość kontrolki.
		 *
		 * @return Wartość JSON o typie zgodnym z rodzajem kontrolki
		 */
		virtual json_spirit::Value getValue() = 0;

		/**
		 * Zwraca nazwę (identyfikator) kontrolki.
		 */
		const std::string getName() const { return name; }

	protected:
		const std::string name;
};

/**
 * Klasa etykiety/okienka z tekstem.
 */
class CDKLabel : public CDKWidget
{
	public:
		CDKLabel(CDKSCREEN* scr, int x, int y, const std::string& text, const std::string& n = "");
		CDKLabel(const std::string& json, CDKSCREEN* scr, int x, int y, const std::string& n = "");
		virtual ~CDKLabel();

		virtual void activate();
		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();

		/**
		 * Tworzy i wyświetla pop-up z danym tekstem.
		 *
		 * @param scr Ekran do wyświetlenia okienka
		 * @param text Tekst do wyświetlenia
		 */
		static void popup(CDKSCREEN* scr, const CDKStringArray& text);

		/**
		 * Tworzy i wyświetla pop-up ze sformatowanymi danymi
		 *
		 * @param scr Ekran do wyświetlenia okienka
		 * @param json Obiekt JSON do wyświetlenia
		 */
		static void popup(CDKSCREEN* scr, const std::string& json);

	private:
		boost::shared_ptr<CDKStringArray> lines;
		CDKLABEL* label;
};

/**
 * Klasa kontrolki typu radio (1 z N).
 */
class CDKRadio : public CDKWidget
{
	public:
		CDKRadio(CDKSCREEN* scr, int x, int y, int w, int h, const std::string& title, const std::string& n = "");
		virtual ~CDKRadio();

		virtual void activate();
		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();

		/**
		 * Ustawia zawartość pól wyboru.
		 *
		 * @param itemList Wektor tytułów pól
		 */
		virtual void setItems(const std::vector<std::string>& itemList);

		/**
		 * Dodaje callback do wykonania po przetworzeniu wejścia.
		 *
		 * @param fn Funkcja o argumentach void* (wakaźnik na wewnętrzny obiekt CDKRADIO) i chtype (wprowadzony znak)
		 */
		void addCallback(std::function<void(void*, chtype c)> fn);

		/**
		 * Zwraca indeks aktualnie wybranego elementu.
		 *
		 * @return Wartość od 0 do N-1 dla N elementów
		 */
		int getSelected();

		/**
		 * Zwraca kod wyjścia z funkcji activate.
		 *
		 * @return Stała definiowana przez CDK: vNORMAL, vESCAPE_HIT
		 */
		int getExitType();

	protected:
		boost::shared_ptr<CDKStringArray> items;
		CDKRADIO* radio;
		int selected;
};


/**
 * Klasa zastępująca checkbox.
 */
class CDKBoolean : public CDKRadio
{
	public:
		CDKBoolean(CDKSCREEN* scr, int x, int y, int w, int h, const std::string& title, const std::string& n = "");
		~CDKBoolean();

		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();
		virtual void setItems(const std::vector<std::string>& itemList);
};

/**
 * Klasa pola tekstowego.
 */
class CDKInput : public CDKWidget
{
	public:
		CDKInput(CDKSCREEN* scr, int y, int w, EDisplayType dtype, bool box, const std::string& title, const std::string& n = "");
		virtual ~CDKInput();

		virtual void activate();
		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();

		/**
		 * Aktualizuje i zwraca bieżącą wartość z pola.
		 *
		 * @return C-string z zawartością pola tekstowego
		 */
		char* getText();

	private:
		CDKENTRY* entry;
		char* text;
};


/**
 * Klasa przycisku.
 */
class CDKButton : public CDKWidget
{
	public:
		CDKButton(CDKSCREEN* scr, int x, int y, const std::string& text, tButtonCallback callback, const std::string& n = "");
		virtual ~CDKButton();

		virtual void activate();
		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();

	private:
		CDKBUTTON* button;
};

/**
 * Klasa kontrolki kalendarza.
 */
class CDKCalendar : public CDKWidget
{
	public:
		CDKCalendar(CDKSCREEN* scr, int y, int year, int month, int day, const std::string& title, const std::string& n = "");
		virtual ~CDKCalendar();

		virtual void activate();
		virtual void setValue(const json_spirit::Value& val);
		virtual json_spirit::Value getValue();

	private:
		CDKCALENDAR* cal;
};


/**
 * Klasa formularza.
 */
class CDKForm
{
	public:
		/**
		 * Konstruktor, tworzy formularz z obiektu JSON.
		 *
		 * @param win Okno curses
		 * @param title Tytuł formularza
		 * @param json Obiekt JSON zawierający obiekt "fields" i tablicę "order"
		 */
		CDKForm(WINDOW* win, const std::string& title, const std::string& json);

		~CDKForm();

		/**
		 * Wypełnia formularz danymi.
		 *
		 * @param json Obiekt JSON zawierający dane
		 */
		void setData(const std::string& json);

		/**
		 * Czeka na wypełnienie i zapisuje dane z formularza.
		 *
		 * @param ret String do zapisania zserializowanych danych
		 * @return true jeśli formularz zapisany przyciskiem OK, false w innych wypadkach
		 */
		bool getData(std::string& ret);

	private:
		/**
		 * Tworzy i dodaje do formularza kontrolkę.
		 *
		 * @param name Nazwa (identyfikator) pola formularza
		 * @param val Obiekt wartośći JSON z opisem typu pola
		 */
		void addWidget(const std::string& name, json_spirit::Value val);

		/**
		 * Zapisuje dane z formularza w formacie JSON.
		 *
		 * @param out String do zapisania zserializowanych danych
		 */
		void serialize(std::string& out);

		std::vector<CDKWidget*> widgets;
		CDKButton* buttons[2];

		unsigned int height;

		CDKSCREEN* screen;
};

#endif
