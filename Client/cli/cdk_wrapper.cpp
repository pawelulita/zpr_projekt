#include <cstring>
#include <ctime>
#include <cstdio>
#include <map>

#include "cdk_wrapper.h"

struct CDKCallbackHandler
{
	static void addPostCallback(void* obj, std::function<void(void*, chtype)> fn)
	{
		postCallbacks[obj].push_back(fn);
	}

	static int genericPostCallback(EObjectType type, void* obj, void* data, chtype input)
	{
		if (!postCallbacks.count(obj))
			return 1;
		for (auto fn : postCallbacks[obj])
			fn(obj, input);
		return 1;
	}

	private:
		CDKCallbackHandler() {}
		static std::map<void*, std::vector<std::function<void(void*, chtype)>>> postCallbacks;
};

std::map<void*, std::vector<std::function<void(void*, chtype)>>> CDKCallbackHandler::postCallbacks;

CDKStringArray::CDKStringArray(const std::string& s)
{
	count = std::count(s.begin(), s.end(), '\n') + 1;
	data = new char*[count];
	for (unsigned int i = 0, cur = 0, next = 0; i < count; i++)
	{
		next = s.find('\n', cur);
		std::string sub = s.substr(cur, next - cur);
		cur = next + 1;
		data[i] = new char[sub.length()+1];
		strcpy(data[i], sub.c_str());
	}
}

CDKStringArray::CDKStringArray(const std::vector<std::string>& lines)
{
	count = lines.size();
	data = new char*[count];
	for (unsigned int i = 0; i < count; i++)
	{
		data[i] = new char[lines[i].length()+1];
		strcpy(data[i], lines[i].c_str());
	}
}

CDKStringArray::~CDKStringArray()
{
	for (unsigned int i = 0; i < count; i++)
		delete[] data[i];

	delete[] data;
}

CDKLabel::CDKLabel(CDKSCREEN* scr, int x, int y, const std::string& text, const std::string& n) : CDKWidget(n), lines(new CDKStringArray(text))
{
	label = newCDKLabel(scr, x, y, lines->data, lines->count, FALSE, FALSE);
}

CDKLabel::CDKLabel(const std::string& json, CDKSCREEN* scr, int x, int y, const std::string& n) : CDKWidget(n)
{
	json_spirit::Value val;
	json_spirit::read(json, val);
	json_spirit::Object obj = val.get_obj();
	std::vector<std::string> newLines;

	const json_spirit::Array& order = json_spirit::find_value(obj, "order").get_array();
	const json_spirit::Object& fields = json_spirit::find_value(obj, "fields").get_obj();

	for (auto i : order)
	{
		for (auto j : fields)
		{
			if (j.name_ == i.get_str())
			{
				newLines.push_back("</B>" + j.name_ + ": <!B>");
				switch (j.value_.type())
				{
					case json_spirit::bool_type:
						newLines.push_back(j.value_.get_bool() ? "true" : "false");
						break;
					case json_spirit::int_type:
						newLines.push_back(std::to_string(j.value_.get_int64()));
						break;
					case json_spirit::real_type:
						newLines.push_back(std::to_string(j.value_.get_real()));
						break;
					case json_spirit::null_type:
						newLines.push_back("");
						break;
					default:
						newLines.push_back(j.value_.get_str());
				}
				break;
			}
		}
	}
	lines.reset(new CDKStringArray(newLines));
	label = newCDKLabel(scr, x, y, lines->data, lines->count, TRUE, FALSE);
}

CDKLabel::~CDKLabel()
{
	destroyCDKLabel(label);
}

void CDKLabel::activate()
{
	activateCDKLabel(label, NULL);
}

void CDKLabel::setValue(const json_spirit::Value& val)
{
	lines.reset(new CDKStringArray(val.get_str()));
	setCDKLabelMessage(label, lines->data, lines->count);
}

json_spirit::Value CDKLabel::getValue()
{
	std::string ret;
	for (unsigned int i = 0; i < lines->count; i++)
		ret += std::string(lines->data[i]);

	return json_spirit::Value(ret);
}

void CDKLabel::popup(CDKSCREEN* scr, const CDKStringArray& text)
{
	popupLabel(scr, text.data, text.count);
}

void CDKLabel::popup(CDKSCREEN* scr, const std::string& json)
{
	CDKLabel p(json, scr, CENTER, CENTER);
	refreshCDKScreen(scr);
	waitCDKLabel(p.label, 0);
}

CDKRadio::CDKRadio(CDKSCREEN* scr, int x, int y, int w, int h, const std::string& title, const std::string& n) : CDKWidget(n), selected(0)
{
	radio = newCDKRadio(scr, x, y, RIGHT, h, w, title.c_str(), NULL, 0, '*' | A_REVERSE, 1, A_REVERSE, TRUE, FALSE);
	setCDKRadioPostProcess(radio, CDKCallbackHandler::genericPostCallback, NULL);
	CDKCallbackHandler::addPostCallback(radio, [](void* obj, chtype c){ setCDKRadioSelectedItem((CDKRADIO*)obj, getCDKRadioCurrentItem((CDKRADIO*)obj)); });
}

CDKRadio::~CDKRadio()
{
	destroyCDKRadio(radio);
}

void CDKRadio::activate()
{
	selected = activateCDKRadio(radio, NULL);
}

void CDKRadio::setValue(const json_spirit::Value& val)
{
	for (unsigned int i = 0; i < items->count; i++)
	{
		if (val.get_str() == std::string(items->data[i]))
		{
			setCDKRadioCurrentItem(radio, selected = i);
			setCDKRadioSelectedItem(radio, selected);
			break;
		}
	}
}

json_spirit::Value CDKRadio::getValue()
{
	return json_spirit::Value(items->data[selected]);
}

void CDKRadio::setItems(const std::vector<std::string>& itemList)
{
	items.reset(new CDKStringArray(itemList));
	setCDKRadioItems(radio, (CDK_CSTRING2) items->data, items->count);
}

void CDKRadio::addCallback(std::function<void(void*, chtype c)> fn)
{
	CDKCallbackHandler::addPostCallback(radio, fn);
}

int CDKRadio::getSelected()
{
	return selected = getCDKRadioSelectedItem(radio);
}

int CDKRadio::getExitType()
{
	return radio->exitType;
}

CDKBoolean::CDKBoolean(CDKSCREEN* scr, int x, int y, int w, int h, const std::string& title, const std::string& n)
	: CDKRadio(scr, x, y, w, h, title, n)
{
	std::vector<std::string> items = {"</16>no<!16>", "</24>yes<!24>"};
	setItems(items);
}

CDKBoolean::~CDKBoolean()
{
}

void CDKBoolean::setValue(const json_spirit::Value& val)
{
	if (val.get_bool())
	{
		setCDKRadioCurrentItem(radio, 1);
		setCDKRadioSelectedItem(radio, 1);
	}
	else
	{
		setCDKRadioCurrentItem(radio, 0);
		setCDKRadioSelectedItem(radio, 0);
	}
}

json_spirit::Value CDKBoolean::getValue()
{
	return json_spirit::Value((bool)getSelected());
}

void CDKBoolean::setItems(const std::vector<std::string>& itemList)
{
	if (itemList.size() != 2)
		return;
	CDKRadio::setItems(itemList);
}

CDKInput::CDKInput(CDKSCREEN* scr, int y, int w, EDisplayType dtype, bool box, const std::string& title, const std::string& n) : CDKWidget(n), text(NULL)
{
	entry = newCDKEntry(scr, CENTER, y, NULL, title.c_str(), A_NORMAL, '_', dtype, w, 0, 1024, box ? TRUE : FALSE, FALSE);
	if (dtype == vHMIXED)
		setCDKEntryHiddenChar(entry, '*');
}

CDKInput::~CDKInput()
{
	destroyCDKEntry(entry);
}

void CDKInput::activate()
{
	text = activateCDKEntry(entry, NULL);
}

void CDKInput::setValue(const json_spirit::Value& val)
{
	switch (val.type())
	{
		case json_spirit::int_type:
			setCDKEntryValue(entry, std::to_string(val.get_int64()).c_str());
			break;
		case json_spirit::real_type:
			setCDKEntryValue(entry, std::to_string(val.get_real()).c_str());
			break;
		case json_spirit::null_type:
			setCDKEntryValue(entry, "");
			break;
		default:
			setCDKEntryValue(entry, val.get_str().c_str());
	}
}

json_spirit::Value CDKInput::getValue()
{
	return json_spirit::Value(getText());
}

char* CDKInput::getText()
{
	return text = getCDKEntryValue(entry);
}

CDKButton::CDKButton(CDKSCREEN* scr, int x, int y, const std::string& text, tButtonCallback callback, const std::string& n) : CDKWidget(n)
{
	button = newCDKButton(scr, x, y, text.c_str(), callback, TRUE, FALSE);
}

CDKButton::~CDKButton()
{
	destroyCDKButton(button);
}

void CDKButton::activate()
{
	activateCDKButton(button, NULL);
}

void CDKButton::setValue(const json_spirit::Value& val)
{
}

json_spirit::Value CDKButton::getValue()
{
	return json_spirit::Value::null;
}

CDKCalendar::CDKCalendar(CDKSCREEN* scr, int y, int year, int month, int day, const std::string& title, const std::string& n) : CDKWidget(n)
{
	time_t tmstamp;
	time(&tmstamp);
	tm* loctm = localtime(&tmstamp);
	cal = newCDKCalendar(scr, CENTER, y, title.c_str(), day ? day : loctm->tm_mday, month ? month : loctm->tm_mon + 1, year ? year : loctm->tm_year + 1900, A_NORMAL, A_BOLD, A_BOLD, A_REVERSE, TRUE, FALSE);
}

CDKCalendar::~CDKCalendar()
{
	destroyCDKCalendar(cal);
}

void CDKCalendar::activate()
{
	activateCDKCalendar(cal, NULL);
}

void CDKCalendar::setValue(const json_spirit::Value& val)
{
	if (val.type() == json_spirit::str_type)
	{
		int d, m, y;
		sscanf(val.get_str().c_str(), "%d-%d-%d", &y, &m, &d);
		setCDKCalendarDate(cal, d, m, y);
	}
}

json_spirit::Value CDKCalendar::getValue()
{
	int d, m, y;
	char buffer[16];
	getCDKCalendarDate(cal, &d, &m, &y);
	sprintf(buffer, "%04d-%02d-%02d", y, m, d);

	return json_spirit::Value(buffer);
}

CDKForm::CDKForm(WINDOW* win, const std::string& title, const std::string& json) : height(3)
{
	screen = initCDKScreen(win);

	json_spirit::Value val;
	json_spirit::read(json, val);
	json_spirit::Object obj = val.get_obj();

	const json_spirit::Object& fields = json_spirit::find_value(obj, "fields").get_obj();
	const json_spirit::Array& order = json_spirit::find_value(obj, "order").get_array();

	widgets.push_back(new CDKLabel(screen, CENTER, 1, "<C></B>" + title + "<!B>", ""));

	for (auto i : order)
	{
		for (auto j : fields)
		{
			if (j.name_ == i.get_str())
			{
				addWidget(j.name_, j.value_);
				break;
			}
		}
	}

	buttons[0] = new CDKButton(screen, LEFT, BOTTOM, " OK ", (tButtonCallback)exitOKCDKScreenOf);
	buttons[1] = new CDKButton(screen, RIGHT, BOTTOM, " Cancel ", (tButtonCallback)exitCancelCDKScreenOf);
}

CDKForm::~CDKForm()
{
	for (auto i : widgets)
		delete i;

	delete buttons[0];
	delete buttons[1];

	destroyCDKScreen(screen);
}

void CDKForm::setData(const std::string& json)
{
	json_spirit::Value val;
	json_spirit::read(json, val);
	json_spirit::Object obj = val.get_obj();

	const json_spirit::Object& fields = json_spirit::find_value(obj, "fields").get_obj();

	for (auto f : fields)
	{
		for (auto w : widgets)
		{
			if (w->getName() == f.name_)
			{
				w->setValue(f.value_);
				break;
			}
		}
	}
}

bool CDKForm::getData(std::string& ret)
{
	if (!widgets.size())
		return 0;
	refreshCDKScreen(screen);

	if (traverseCDKScreen(screen))
	{
		eraseCDKScreen(screen);
		serialize(ret);
		return true;
	}

	eraseCDKScreen(screen);
	ret = "";
	return false;
}

void CDKForm::serialize(std::string& out)
{
	json_spirit::Object obj;

	for (auto w : widgets)
	{
		if (w->getName() != "")
			obj.push_back(json_spirit::Pair(w->getName(), w->getValue()));
	}

	out = json_spirit::write(obj);
}

void CDKForm::addWidget(const std::string& name, json_spirit::Value val)
{
	if (val.type() == json_spirit::str_type)
	{
		if (val.get_str() == "string")
		{
			widgets.push_back(new CDKInput(screen, height, 60, vMIXED, false, "</B>" + name + ": <!B>", name));
			height += 3;
		}
		else if (val.get_str() == "bool")
		{
			widgets.push_back(new CDKBoolean(screen, CENTER, height, 5, 5, "<C></B>" + name + ": <!B>", name));
			height += 5;
		}
		else if (val.get_str() == "int")
		{
			widgets.push_back(new CDKInput(screen, height, 60, vINT, false, "</B>" + name + ": <!B>", name));
			height += 3;
		}
		else if (val.get_str() == "date")
		{
			widgets.push_back(new CDKCalendar(screen, height, 0, 0, 0, "<C></B>" + name + ": <!B>", name));
			height += 15;
		}
		else if (val.get_str() == "password")
		{
			widgets.push_back(new CDKInput(screen, height, 60, vHMIXED, false, "</B>" + name + ": <!B>", name));
			height += 3;
		}
	}
}
