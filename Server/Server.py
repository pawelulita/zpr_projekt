"""
Serwer naszej pieknej aplikacji.
"""
import importlib
import os.path
import os

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from py.ZPR_Projekt import ZPR_Projekt


class ZPRProjektHandler(object):
    """
    Klasa definiujaca wszelkie zachowania serwera.
    """

    def __init__(self):
        self.lock = False  # pseudo mutex
        self.active_plugin = None
        self.services = self._find_services()

    def _find_services(self):
        """
        Znajdz pluginy i umozliwij ich uzycie. Funkcja przeszukuje folder
        'plugins' w poszukiwaniu plikow *.py (z wylaczeniem __init__.py).
        Te pliki sa dodawane do listy dostepnych pluginow.

        :return: Slownik w ktorym kluczem jest nazwa pluginu (serwisu),
        a wartoscia modul zawierajacy klase Plugin realizujaca wsparcie
        dla tego serwisu.
        """
        root_dir = os.path.split(os.path.realpath(__file__))[0]
        plugin_dir = os.path.join(root_dir, 'plugins')

        file_filter = lambda x: x[-2:] == 'py' and x != '__init__.py'
        plugin_files = filter(file_filter, os.listdir(plugin_dir))
        plugin_names = [f[:-3] for f in plugin_files]
        services = {p: importlib.import_module('plugins.' + p) for p in plugin_names}

        return services

    def connect(self, ):
        """
        Podlacz sie do serwera.
        :return: Czy udalo sie podlaczyc.
        """
        if self.lock:
            return False
        else:
            self.lock = True
            return True

    def disconnect(self, ):
        """
        Zakoncz sesje z serwerem.
        :return: Czy udalo sie zakonczyc sesje.
        """
        result = False

        if self.lock:
            self.lock = False
            result = True

        return result

    def listServices(self, ):
        """
        Pobierz dostepne uslugi.
        :return: Lista dostepnych uslug.
        """
        return self.services.keys()

    def getLogInForm(self, serv):
        """
        Pobierz formularz do zalogowania.
        :param serv: Wybrana usluga.
        :return: dane potrzebne do zalogowania.
        """
        try:
            plugin = self.services[serv].Plugin()
            form = plugin.get_login_form()
        except KeyError:
            form = ""

        return form

    def logIn(self, params, serv):
        """
        Zaloguj sie.
        :param params: Dane do zalogowania.
        :param serv: Wybrana usluga.
        :return:
        """
        self.active_plugin = self.services[serv].Plugin()
        return self.active_plugin.log_in(params)

    def logOut(self, ):
        """
        Wyloguj sie.
        """
        return self.active_plugin.log_out()

    def getNewProjectForm(self, ):
        """
        Pobierz formularz do nowego projektu.
        :return: JSON: fields + order.
        """
        return self.active_plugin.get_new_project_form()

    def newProject(self, params):
        """
        Utworz nowy projekt.
        :param params: Parametry nowego projektu.
        """
        return self.active_plugin.new_project(params)

    def listProjects(self, ):
        """
        Pobierz liste projektow.
        :return: JSON: fields ("ID", "Nazwa")
        """
        return self.active_plugin.list_projects()

    def getProject(self, proj_id):
        """
        Poberz projekt o zadanym ID.
        :param proj_id: ID projektu do pobrania.
        :return: JSON: fields + order ('Nazwa', 'Opis', 'Utworzony', 'Zmodyfikowany')
        """
        return self.active_plugin.get_project(proj_id)

    def editProject(self, params, proj_id):
        """
        Zaktualizuj projekt.
        :param proj_id: ID projektu.
        :param params: Nowe parametry projektu.
        """
        return self.active_plugin.edit_project(proj_id, params)

    def getNewTasksForm(self, ):
        """
        Funkcja zwraca formularz dla nowego zadania.
        :return: JSON: fields + order.
        """
        return self.active_plugin.get_new_task_form()

    def newTask(self, params, proj_id):
        """
        Funkcja dodaje nowe zadanie.
        :param proj_id: ID projektu, do ktorego dodawane jest zadanie.
        :param params: Parametry nowego zadania.
        :return: Wynik logiczny operacji.
        """
        return self.active_plugin.new_task(params, proj_id)

    def listTasks(self, proj_id):
        """
        Pobierz zadania z tego projektu.
        :param proj_id: ID projektu.
        :return: Lista JSON-ow: fields + order ('Nazwa', 'ID').
        """
        return self.active_plugin.list_tasks(proj_id)

    def editTask(self, params, task_id):
        """
        Funkcja aktualizujaca zadanie o zadanym ID.
        :param task_id: ID zadania.
        :param params: Nowe parametry zadania.
        :return: Wynik logiczny operacji.
        """
        return self.active_plugin.edit_task(task_id, params)

    def getTask(self, task_id):
        """
        Funkcja pobierajaca zadanie o zadanym ID.
        :param task_id: ID zadania.
        :return: JSON: fields + order.
        """
        return self.active_plugin.get_task(task_id)


if __name__ == '__main__':
    handler = ZPRProjektHandler()
    processor = ZPR_Projekt.Processor(handler)
    transport = TSocket.TServerSocket(port=9090)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

    print "odpalam serwer"
    try:
        server.serve()
    except KeyboardInterrupt:
        pass
    print "koniec"
