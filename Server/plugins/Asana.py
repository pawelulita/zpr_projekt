"""
Modul zawierajacy klase obslugujaca serwis Asana.
"""
import json
from Asana_plugin import asana


# noinspection PyBroadException
class Plugin(object):
    """
    Plugin obslugujacy serwis Asana.
    """

    def __init__(self):
        self.api = None

    def _reformat_date(self, date):
        """
        Usun niepotrzebne dane z dany zwracanej przez serwis Asana.
        :param date: Data do przeformatowania.

        :return: Przeformatowana data.
        """
        return date[:10] + ' ' + date[11:16]

    def get_login_form(self):
        """
        Pobierz formularz do zalogowania. W tym wypadku jest to
        tylko jedno pole: Asana API key.

        :return: Formularz do zalogowania.
        """
        form = {
            'fields': {
                "Asana API key": "password"
            },
            'order': ["Asana API key"]
        }
        return json.dumps(form)

    def log_out(self):
        """
        Wyloguj sie. Po wykonaniu tej funkcji wszelkie odwolania
        do api beda skutkowaly zwroceniem pustych danych.

        :return: True jezeli udalo sie zalogowac, a
                 False w przeciwnym wypadku.
        """
        if self.api:
            self.api = None
            return True
        else:
            return False

    def log_in(self, params):
        """
        Zaloguj sie do serwisu. Wymaga podania Asana API key.

        :param params: Parametry do zalogowania w JSON.
        :return: True, jezel udalo sie zalogowac.
                 False w przeciwnym wypadku.
        """
        try:
            self.log_out()
            params = json.loads(params)
            apikey = params['Asana API key']
            self.api = asana.AsanaAPI(apikey)

            if self.api.check_api_key():
                self.workspace = self.api.list_workspaces()[0]['id']
                result = True
            else:
                result = False
                self.log_out()
        except:
            result = False

        return result

    def get_new_project_form(self):
        """
        Pobierz formatke do nowego projektu.

        :return: Formatka do nowego projektu.
        """
        form = {
            "fields": {
                "Nazwa": "string",
                "Opis": "string"
            },
            "order": ["Nazwa", "Opis"]
        }
        return json.dumps(form)

    def new_project(self, params):
        """
        Utworz nowy projekt. Jedyne dostepne parametry
        to te, ktore byly w formatce z get_new_project_form.

        :param params: Parametry nowego projektu.
        :return: True jezeli udalo sie dodac,
                 False w przeciwnym wypadku.
        """
        try:
            params = json.loads(params)
            self.api.create_project(name=params["Nazwa"],
                                    workspace=self.workspace, notes=params['Opis'])
            result = True
        except:
            result = False

        return result

    def list_projects(self):
        """
        Wypisz projekty.

        :return: Spis projektow zgodnie z opisem w serwerze.
        """
        try:
            projects = self.api.list_projects()
            ren_keys = lambda x: {'ID': x['id'], 'Nazwa': x['name']}
            projects[:] = map(ren_keys, projects)
            projects = [json.dumps(x) for x in projects]
        except:
            projects = []

        return projects

    def get_project(self, proj_id):
        """
        Pobierz projekt.

        :param proj_id: ID projektu.
        :return: Dane zgodnie z opisem serwera.
        """
        try:
            project = self.api.get_project(proj_id)
            project = {'fields': {'Nazwa': project['name'], 'Opis': project['notes'],
                                  "Utworzony": self._reformat_date(project['created_at']),
                                  'Zmodyfikowany':
                                      self._reformat_date(project['modified_at'])
            },
                       'order': ['Nazwa', 'Opis', 'Utworzony', 'Zmodyfikowany']
            }
            project = json.dumps(project)
        except:
            project = ""

        return project

    def edit_project(self, proj_id, params):
        """
        Zmodyfikuj dane projektu. Dane do zmodyfikowania sa
        takie same jak te przy dodawaniu.

        :param proj_id: ID projektu.
        :param params: Parametry zgodnie z get_new_project_form.
        :return: True jezeli udalo sie edytowac projekt,
                 False w przeciwnym wypadku.
        """
        try:
            proj_id = int(proj_id)
            params = json.loads(params)
            self.api.update_project(proj_id, params['Nazwa'], params['Opis'])
            result = True
        except:
            result = False

        return result

    def get_new_task_form(self):
        """
        Formularz do nowego zadania.

        :return: Odpowiedni formularz.
        """
        form = {
            "fields": {
                "Nazwa": 'string',
                'Opis': 'string',
                'ID przydzielonego': 'int',
                'Data zakonczenia': 'date',
                'Zakonczony': 'bool'
            },
            'order': ['Nazwa', 'ID przydzielonego', 'Opis',
                      'Data zakonczenia', 'Zakonczony']
        }
        return json.dumps(form)

    def list_tasks(self, proj_id):
        """
        Wypisz zadania przypisane do danego projektu.

        :param proj_id: ID projektu.
        :return: Zadania zgodnie z opisem serwera.
        """
        try:
            tasks = self.api.list_tasks(proj_id)
            filter_task_data = lambda x: {'fields': {'Nazwa': x['name'], 'ID': x['id']},
                                          'order': ['Nazwa', 'ID']}
            tasks[:] = map(filter_task_data, tasks)
            tasks = [json.dumps(x) for x in tasks]
        except:
            tasks = []

        return tasks

    def new_task(self, params, proj_id):
        """
        Utworz nowe zadanie.

        :param params: Parametry zgodnie z tym, co zwraca
                       get_new_task_form.
        :param proj_id: ID projektu, do ktorego ma byc
                        przypisane zadanie.
        :return: True jezeli dodanie sie powiedzie,
                 False w przeciwnym wypadku.
        """
        try:
            self.api.get_project(proj_id)
            params = json.loads(params)
            task = self.api.create_task(
                name=params['Nazwa'], workspace=self.workspace,
                assignee=params['ID przydzielonego'], due_on=params['Data zakonczenia'],
                completed=params['Zakonczony'], notes=params['Opis']
            )
            self.api.add_project_task(task['id'], proj_id)
            result = True
        except:
            result = False

        return result

    def edit_task(self, task_id, params):
        """
        Edytuj zadanie. Dostepne pola zgodnie z
        get_new_task_form.

        :param task_id: ID zadania.
        :param params: Nowe parametry.
        :return: True jezeli udalo sie zmienic zadanie,
                 False w przeciwnym wypadku.
        """
        try:
            task_id = int(task_id)
            params = json.loads(params)
            self.api.update_task(
                task=task_id, name=params['Nazwa'], assignee=params['ID przydzielonego'],
                due_on=params['Data zakonczenia'], completed=params['Zakonczony'],
                notes=params['Opis']
            )
            result = True
        except:
            result = False

        return result

    def get_task(self, task_id):
        """
        Pobierz zadanie. Nie wszystkie pola mozliwe
        do edycji.

        :param task_id: ID zadania.
        :return: Dane o zadaniu.
        """
        task_id = int(task_id)
        task = self.api.get_task(task_id)
        task = {'fields': {'Nazwa': task['name'],
                           'ID przydzielonego': (task['assignee']['id']
                                                 if task['assignee'] else ""),
                           'Opis': task['notes'],
                           'Data utworzenia': self._reformat_date(task['created_at']),
                           'Data modyfikacji': self._reformat_date(task['modified_at']),
                           'Data zakonczenia': task['due_on'],
                           'Zakonczony': task['completed'],
                           'ID projektu': (task['projects'][0]['id']
                                           if task['projects'] else None)
        }, 'order': ['Nazwa', 'ID przydzielonego', 'Opis',
                     'ID projektu', 'Data utworzenia', 'Data modyfikacji',
                     'Data zakonczenia', 'Zakonczony']}
        task = json.dumps(task)

        return task
