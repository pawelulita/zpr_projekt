service ZPR_Projekt {
    bool connect(),
    bool disconnect(),
    list<string> listServices(),
    string getLogInForm(1:string serv),
    bool logIn(1:string params, 2:string serv),
    bool logOut(),
    string getNewProjectForm(),
    bool newProject(1:string params),
    list<string> listProjects(),
    string getProject(1:i64 projID),
    bool editProject(1:string params, 2:i64 projID),
    string getNewTasksForm(),
    bool newTask(1:string params, 2:i64 projID),
    list<string> listTasks(1:i64 projID),
    bool editTask(1:string params, 2:i64 taskID),
    string getTask(1:i64 taskID)
}
